output "public_id" {
  description = "Public subnet IDs"
  value       = module.vpc.public_subnets
}

output "eks_platform_version" {
  description = "Version"
  value       = aws_eks_cluster.eks-cluster.version
}

output "labUser_instance-profile" {
  description = "Instance Profile"
  value       = data.aws_iam_instance_profiles.example.names
}