# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "iam_role" {
  description = "IAM Role"
  type        = string
  default     = "arn:aws:iam::851725473879:role/LabRole"
}

variable "ec2_key" {
  description = "EC2 Key"
  type        = string
  default     = "vockey"
}

# resource "aws_key_pair" "ec2_key" {
#   key_name   = "ssh_key"
#   public_key = file("~/.ssh/id_rsa.pub")
# }
